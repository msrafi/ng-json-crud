import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../../shared/employee.service';
import { Employee } from 'src/app/model/employee.model';

@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.scss']
})
export class EmployeeListComponent implements OnInit {

  allEmployee : Employee[];

  constructor(private employeeService: EmployeeService) { }

  ngOnInit() {
    this.getAllEmployeeList();
  }

  getAllEmployeeList(){
    this.employeeService.getAllEmployees().subscribe(
      (data:Employee[]) => {
        this.allEmployee = data;
        console.log(data);
      }
    );
  }

  deleteEmployee(id:number){
    this.employeeService.deleteCurrentEmployee(id).subscribe(
      (data:Employee) => {
        this.getAllEmployeeList();
      }
    );
  }

  edit(emp){
    this.employeeService.currentEmployee = Object.assign({}, emp);
  }

}

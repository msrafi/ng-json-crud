import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../../shared/employee.service';
import { Employee } from 'src/app/model/employee.model';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.scss']
})
export class EmployeeComponent implements OnInit {

  constructor(public employeeService: EmployeeService) { }

  ngOnInit() {
  }

  createAndUpdate(currentEmployee:Employee){
    if(currentEmployee.id !== null){
      this.updateEmployee(currentEmployee);
    }else{
      this.createEmployee(currentEmployee);
    };
  }

  updateEmployee(currentEmployee){
    this.employeeService.updateEmployee(currentEmployee).subscribe();
  }

  createEmployee(currentEmployee){
    this.employeeService.createEmployee(currentEmployee).subscribe();
  }

  clear(){
    this.employeeService.currentEmployee = {
      id: null,
      firstName: '',
      lastName: '',
      designation: '',
      contact: null,
      address: ''
    }
  }

}
